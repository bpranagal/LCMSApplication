package pl.pranagal.bartosz.lcmsapp.group.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pranagal.bartosz.lcmsapp.group.model.Group;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    boolean existsByNameAllIgnoreCase(String name);

    Optional<Group> getByNameIgnoreCase(String name);


}
