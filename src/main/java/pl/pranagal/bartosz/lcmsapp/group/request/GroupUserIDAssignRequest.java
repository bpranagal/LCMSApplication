package pl.pranagal.bartosz.lcmsapp.group.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GroupUserIDAssignRequest{
    private String userId;
}
