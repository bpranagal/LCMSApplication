package pl.pranagal.bartosz.lcmsapp.group.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class GroupRequest {
    private String name;
    private List<String> users;
}
