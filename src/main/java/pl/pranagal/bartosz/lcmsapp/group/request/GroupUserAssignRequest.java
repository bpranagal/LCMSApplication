package pl.pranagal.bartosz.lcmsapp.group.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.common.AssignTypeEnum;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupUserAssignRequest{
    private String userId;
    private String groupId;
    private AssignTypeEnum assignTypeEnum;
}
