package pl.pranagal.bartosz.lcmsapp.group.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.common.AssignTypeEnum;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class GroupAssignRequestWrapper {
    private String groupId;
    private AssignTypeEnum assignType;
    private List<String> users;
}
