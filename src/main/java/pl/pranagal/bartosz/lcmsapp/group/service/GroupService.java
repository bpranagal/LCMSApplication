package pl.pranagal.bartosz.lcmsapp.group.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pranagal.bartosz.lcmsapp.common.AssignTypeEnum;
import pl.pranagal.bartosz.lcmsapp.common.PermissionsService;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.exception.topic.TopicAlreadyExistsException;
import pl.pranagal.bartosz.lcmsapp.group.model.Group;
import pl.pranagal.bartosz.lcmsapp.group.repository.GroupRepository;
import pl.pranagal.bartosz.lcmsapp.group.request.*;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.service.UserService;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service
@RequiredArgsConstructor
public class GroupService {
    private final GroupRepository groupRepository;
    private final PermissionsService permissionsService;
    private final UserService userService;

    public List<Group> getAll() {
        return groupRepository.findAll();
    }

    @Transactional
    public Group createGroup(GroupRequest groupRequest, Authentication authentication) {
        permissionsService.checkGroupAssignerPermissions(authentication);
        checkGroupExistenceByTitle(groupRequest.getName());

        Group group = new Group();
        group.setName(groupRequest.getName());
        groupRepository.save(group);

        Set<GroupUserAssignRequest> userIds = groupRequest.getUsers().stream()
                .map(id -> new GroupUserAssignRequest(id, group.getId().toString(), AssignTypeEnum.ASSIGN))
                .collect(Collectors.toSet());

        Map<String, Set<GroupUserAssignRequest>> map = new HashMap<>();
        map.put(group.getId().toString(), userIds);

        assignUsersFromMap(map);

        return group;
    }

    private void checkGroupExistenceByTitle(String name) {
        if (groupRepository.existsByNameAllIgnoreCase(name)) {
            throw new TopicAlreadyExistsException(name + ", topic with given name already exists.");
        }
    }

    @Transactional
    public void assignUsers(GroupAssignRequestWrapper groupAssignRequestWrapper, Authentication authentication) {
        permissionsService.checkGroupAssignerPermissions(authentication);

        List<GroupUserAssignRequest> userAssignRequests = groupAssignRequestWrapper.getUsers().stream()
                .map(id ->
                        new GroupUserAssignRequest(id, groupAssignRequestWrapper.getGroupId(), AssignTypeEnum.ASSIGN)
                ).collect(Collectors.toList());

        GroupAssignRequest groupAssignRequest = new GroupAssignRequest();
        groupAssignRequest.setUsersAssign(userAssignRequests);

        Map<String, Set<GroupUserAssignRequest>> groupsUsers = groupAssignRequest.getUsersAssign()
                .stream()
                .collect(Collectors.groupingBy(
                        GroupUserAssignRequest::getGroupId,
                        Collectors.mapping(gr -> new GroupUserAssignRequest(gr.getUserId(), gr.getGroupId(), gr.getAssignTypeEnum() == null ? AssignTypeEnum.ASSIGN : gr.getAssignTypeEnum()), toSet()))
                );

        assignUsersFromMap(groupsUsers);
    }


    private void assignUsersFromMap(Map<String, Set<GroupUserAssignRequest>> groupsUsers) {
        List<Group> groups = new ArrayList<>();

        groupsUsers.forEach((key, value) ->
                assignOrRemoveAndAddToList(groups, key, value)
        );

        groupRepository.saveAll(groups);
    }

    private void assignOrRemoveAndAddToList(List<Group> groups, String key, Set<GroupUserAssignRequest> value) {
        Group group = findById(key);

        List<Long> userIds = value.stream()
                .map(e -> Long.parseLong(e.getUserId()))
                .collect(toList());

        List<UserEntity> users = userService.findAllById(userIds);

        value.forEach(assignRequest -> {
            Optional<UserEntity> user = users.stream().filter(u -> u.getId().equals(Long.parseLong(assignRequest.getUserId()))).findFirst();
            user.ifPresent(u -> {
                if (assignRequest.getAssignTypeEnum().equals(AssignTypeEnum.ASSIGN))
                    group.addUser(u);
                else
                    group.removeUser(u);
            });
        });

        groups.add(group);
    }

    public Group findById(String id) {
        return groupRepository.findById(Long.parseLong(id))
                .orElseThrow(RuntimeException::new);
    }
}
