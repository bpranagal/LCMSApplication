package pl.pranagal.bartosz.lcmsapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.pranagal.bartosz.lcmsapp.user.model.UserRoleEnum;
import pl.pranagal.bartosz.lcmsapp.user.request.UserCreateRequest;
import pl.pranagal.bartosz.lcmsapp.user.response.UserGroupResponse;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;
import pl.pranagal.bartosz.lcmsapp.user.request.UserEditRequest;
import pl.pranagal.bartosz.lcmsapp.user.service.UserService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_ASSIGN_ROLE_ADMIN;
import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_USER;

@RestController
@RequiredArgsConstructor
@RequestMapping(ROUTE_USER)
public class UserController {
    private final UserService userService;


    @GetMapping("/grouped")
    public ResponseEntity<List<UserGroupResponse>> getUsersByGroup(){
        return new ResponseEntity<>(userService.getGroupedUsers(), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<UserResponse> createUser(@RequestBody @Valid UserCreateRequest user) {
        return new ResponseEntity<>(userService.saveUser(user), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> updateUser(@RequestBody UserEditRequest request, @PathVariable("id") Long id){
        return new ResponseEntity<>(userService.updateUser(id,request), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserResponse>> getAllUsers(){
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping(ROUTE_ASSIGN_ROLE_ADMIN + "{id}")
    public ResponseEntity<?> assignRoleToUser(@PathVariable("id") Long id, @RequestParam UserRoleEnum userRole){
        userService.assignRole(id, userRole);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/image")
    public ResponseEntity<?> uploadImage(@ApiIgnore Authentication authentication, @RequestParam("file") MultipartFile multipartFile) throws IOException {
        userService.addImage(multipartFile, authentication);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
