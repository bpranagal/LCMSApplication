package pl.pranagal.bartosz.lcmsapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.pranagal.bartosz.lcmsapp.course.common.CoursePermission;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.course.request.CourseAssignRequest;
import pl.pranagal.bartosz.lcmsapp.course.request.CourseRequest;
import pl.pranagal.bartosz.lcmsapp.course.response.CourseResponse;
import pl.pranagal.bartosz.lcmsapp.course.response.CourseWithoutTopicsResponse;
import pl.pranagal.bartosz.lcmsapp.course.service.CourseService;
import pl.pranagal.bartosz.lcmsapp.exception.course.CourseTitleExistsException;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(ROUTE_COURSE)
public class CourseController {
    private final CourseService courseService;

    //TODO kursy dla usera po tokenie
    //admin wzsystkie
    //nauczyciel i student do ktorych ma dostep

    @GetMapping
    public ResponseEntity<List<CourseWithoutTopicsResponse>> getAllCourses(){
        return new ResponseEntity<>(courseService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CourseResponse> getCourseWithTopic(@PathVariable("id") String id, @ApiIgnore Authentication authentication){
        return new ResponseEntity<>(courseService.getCourseWithTopics(id, authentication), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Map<CoursePermission, List<UserResponse>>> getAllCourseUsers(@PathVariable("id") String id){
        return new ResponseEntity<>(courseService.getAllCourseUsers(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Course> createCourse(@RequestBody @Valid CourseRequest courseRequest, @ApiIgnore Authentication authentication) throws CourseTitleExistsException {
        return new ResponseEntity<>(courseService.createCourse(courseRequest, null, (String) authentication.getPrincipal()), HttpStatus.CREATED);
    }

    @PostMapping("/image/{id}")
    public ResponseEntity<byte[]> uploadImageToCourse(@PathVariable("id") String id, @RequestParam("file") MultipartFile multipartFile, @ApiIgnore Authentication authentication) throws CourseTitleExistsException {
        return new ResponseEntity<>(courseService.uploadImage(id, multipartFile, (String) authentication.getPrincipal()), HttpStatus.CREATED);
    }

    @PutMapping(ROUTE_COURSE_ASSIGN)
    public ResponseEntity<?> manageUsers(@RequestBody CourseAssignRequest request, @ApiIgnore Authentication authentication){
        courseService.manageUsers(request, (String) authentication.getPrincipal());
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable("id") String id){
        courseService.deleteCourse(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<CourseResponse> updateCourse(@RequestBody CourseRequest request, @PathVariable("id")Long id) throws CourseTitleExistsException {
        return new ResponseEntity<>(courseService.updateCourse(id,request),HttpStatus.OK);
    }
}
