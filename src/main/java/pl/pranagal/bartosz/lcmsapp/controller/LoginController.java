package pl.pranagal.bartosz.lcmsapp.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.*;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.common.LoginRequest;
import pl.pranagal.bartosz.lcmsapp.common.LoginResponse;
import pl.pranagal.bartosz.lcmsapp.user.repository.UserRepository;
import pl.pranagal.bartosz.lcmsapp.user.request.UserRegisterRequest;
import pl.pranagal.bartosz.lcmsapp.user.service.UserService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.*;

@RestController
@RequiredArgsConstructor
public class LoginController {
   private final AuthenticationManager authenticationManager;
   private final UserService userService;
   private final UserRepository userRepository;

   @PostMapping(ROUTE_REGISTER)
   public ResponseEntity<UserResponse> registerUser(@RequestBody @Valid UserRegisterRequest userRegisterRequest){
       return new ResponseEntity<>(userService.registerUser(userRegisterRequest), HttpStatus.CREATED);
   }

    @PostMapping(ROUTE_LOGIN)
    public ResponseEntity<LoginResponse> loginUser(@RequestBody LoginRequest loginRequest){
        try {
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginRequest.getUsername(),
                    loginRequest.getPassword()));

            User userDetails = (User) authenticate.getPrincipal();

            UserEntity user = userRepository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new UsernameNotFoundException(userDetails.getUsername()));

            Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
            String token = JWT.create()
                    .withSubject(user.getUsername())
                    .withIssuer("Eminem")
                    .withClaim("roles", List.of(user.getAuthority().getName()))
                    .sign(algorithm);

            LoginResponse loginResponse = new LoginResponse(token, user.getUsername(), user.getEmail(), user.getAuthority().getName(), user.getImage());
            return ResponseEntity.ok(loginResponse);
        } catch (UsernameNotFoundException e){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @GetMapping
    public boolean checkRole(SecurityContextHolderAwareRequestWrapper securityContextHolderAwareRequestWrapper){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Collection<SimpleGrantedAuthority> list = (Collection<SimpleGrantedAuthority>) auth.getAuthorities();

        return securityContextHolderAwareRequestWrapper.isUserInRole(ROLE_ADMIN);
    }
}
