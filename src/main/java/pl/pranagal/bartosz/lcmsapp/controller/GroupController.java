package pl.pranagal.bartosz.lcmsapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.pranagal.bartosz.lcmsapp.group.model.Group;
import pl.pranagal.bartosz.lcmsapp.group.request.GroupAssignRequest;
import pl.pranagal.bartosz.lcmsapp.group.request.GroupAssignRequestWrapper;
import pl.pranagal.bartosz.lcmsapp.group.request.GroupRequest;
import pl.pranagal.bartosz.lcmsapp.group.service.GroupService;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_GROUP;

@RestController
@RequestMapping(ROUTE_GROUP)
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;

    @GetMapping
    public ResponseEntity<List<Group>> getAllGroups(){
        return new ResponseEntity<>(groupService.getAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Group> createGroup(@RequestBody GroupRequest groupRequest, @ApiIgnore Authentication authentication){
        return new ResponseEntity<>(groupService.createGroup(groupRequest, authentication), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> assignUsers(@RequestBody GroupAssignRequestWrapper groupAssignRequest, @ApiIgnore Authentication authentication){
        groupService.assignUsers(groupAssignRequest, authentication);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
