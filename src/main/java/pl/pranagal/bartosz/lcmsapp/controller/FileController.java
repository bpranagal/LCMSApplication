package pl.pranagal.bartosz.lcmsapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.file.FileStorageAdapter;
import pl.pranagal.bartosz.lcmsapp.file.FileStorageService;

import java.io.IOException;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.*;

@RestController
@RequiredArgsConstructor
public class FileController {
    private final FileStorageAdapter fileStorageAdapter;
    private final FileStorageService fileStorageService;

    @GetMapping(ROUTE_FILE_GET)
    public ResponseEntity<byte[]> getFile(@RequestParam String filename) throws IOException {
        FileModel fileModel = fileStorageAdapter.getFileModel(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileModel.getFileName() + "\"")
                .body(fileModel.getData());
    }

    @GetMapping(ROUTE_IMAGE_GET)
    public ResponseEntity<byte[]> getImage(@RequestParam String filename) throws IOException {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
                .body(fileStorageAdapter.getFile(filename));
    }

    @DeleteMapping(ROUTE_DELETE_FILE)
    public ResponseEntity<?> deleteFile(@RequestParam String filename, @RequestParam Long topicId){
        fileStorageService.deleteFile(filename, topicId);
        return ResponseEntity.noContent().build();
    }

}
