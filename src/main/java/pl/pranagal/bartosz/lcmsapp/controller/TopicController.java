package pl.pranagal.bartosz.lcmsapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.file.FileResponse;
import pl.pranagal.bartosz.lcmsapp.topic.request.TopicRequest;
import pl.pranagal.bartosz.lcmsapp.topic.response.TopicResponse;
import pl.pranagal.bartosz.lcmsapp.topic.service.TopicService;
import pl.pranagal.bartosz.lcmsapp.topic.task.Task;
import pl.pranagal.bartosz.lcmsapp.user.request.UserEditRequest;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_TOPIC;

@RestController
@RequestMapping(ROUTE_TOPIC)
@RequiredArgsConstructor
public class TopicController {
    private final TopicService topicService;

    @GetMapping
    public ResponseEntity<List<TopicResponse>> getAll() {
        return new ResponseEntity<>(topicService.getAll(), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<TopicResponse> createTopic(@RequestBody TopicRequest topicRequest, @ApiIgnore Authentication authentication) {
        return new ResponseEntity<>(topicService.createTopic(topicRequest, authentication), HttpStatus.CREATED);
    }

    @PostMapping("/upload/image/{id}")
    public ResponseEntity<byte[]> uploadImageToTopic(@PathVariable("id") String id, @RequestParam("file") MultipartFile multipartFile) throws IOException {
        return new ResponseEntity<>(topicService.uploadImageToTopic(multipartFile, id), HttpStatus.CREATED);
    }

    @PostMapping("/upload/file/{id}")
    public ResponseEntity<List<FileResponse>> uploadFileToTopic(@PathVariable("id") String id, @RequestParam("files") MultipartFile[] multipartFile) {
        return new ResponseEntity<>(topicService.uploadFilesToTopic(multipartFile, id), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<List<FileResponse>> deleteTopic(@PathVariable("id") String id) {
        topicService.deleteTopic(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/task/{id}")
    public ResponseEntity<TopicResponse> createTaskForTopic(@RequestBody Task task, @PathVariable("id") String id){
        return new ResponseEntity<>(topicService.createTask(task, id), HttpStatus.CREATED);
    }

    @DeleteMapping("/task/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable("id") String id){
        topicService.deleteTask(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> updateTopic(@RequestBody TopicRequest request, @PathVariable("id") Long id){
        topicService.updateTopic(id,request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
