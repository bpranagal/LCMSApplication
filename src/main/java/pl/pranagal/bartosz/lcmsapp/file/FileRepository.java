package pl.pranagal.bartosz.lcmsapp.file;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<FileModel, Long> {
    FileModel findByDownloadId(String downloadId);

    FileModel findByFileNameAndTopicId(String filename, Long id);
}
