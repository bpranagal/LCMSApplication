package pl.pranagal.bartosz.lcmsapp.file;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
public class FileUploadRequest {
    private final MultipartFile file;
    private final String topicName;
}
