package pl.pranagal.bartosz.lcmsapp.file;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.topic.model.Topic;

import javax.persistence.*;
import java.nio.file.Path;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class FileModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    private String fileName;

    private String downloadId;

    @JsonIgnore
    private byte[] data;

    @ManyToOne
    private Topic topic;

    @OneToOne
    private Course course;
}
