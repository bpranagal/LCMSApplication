package pl.pranagal.bartosz.lcmsapp.file;


import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class FileStorageService implements FileStorage {
    private final FileRepository fileRepository;

    public FileModel storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
//                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            String downloadId = Base64.getEncoder().encodeToString(ArrayUtils.addAll(fileName.getBytes(), ZonedDateTime.now().toString().getBytes()));
            FileModel fileModel = new FileModel();
            fileModel.setFileName(fileName);
            fileModel.setData(file.getBytes());
            fileModel.setDownloadId(downloadId);

            fileRepository.save(fileModel);
            return fileModel;
        } catch (IOException ex) {
            throw new RuntimeException("File not found " + fileName, ex);
//            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public FileModel loadFileAsResource(String fileName) {
        FileModel fileModel = fileRepository.findByDownloadId(fileName);
        return fileModel;
    }

    public void deleteFile(String filename, Long topicId) {
        FileModel  fileModel = fileRepository.findByFileNameAndTopicId(filename, topicId);
        fileRepository.delete(fileModel);
    }

}
