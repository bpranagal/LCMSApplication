package pl.pranagal.bartosz.lcmsapp.file;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorage {
    public FileModel storeFile(MultipartFile file);
    public FileModel loadFileAsResource(String fileName);
}
