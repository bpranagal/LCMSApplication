package pl.pranagal.bartosz.lcmsapp.file;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class FileMapper {

    @Autowired
    private FileRepository fileRepository;

    @Mapping(source = "downloadId", target = "fileDownloadUri", qualifiedByName = "getFileLink")
    public abstract FileResponse modelToResponse(FileModel fileModel);

    @Named("getFileLink")
    public String getFileLink(String downloadId){
        return FileLinkParser.getFileLinkFromDownloadId(downloadId);
    }

    @Named("getImage")
    public byte[] getImage(String fileDownloadId){
        if(fileDownloadId != null){
            return fileRepository.findByDownloadId(fileDownloadId).getData();
        }else{
            return new byte[]{};
        }
    }
}
