package pl.pranagal.bartosz.lcmsapp.file;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class FileStorageAdapter {
    private final FileStorageService fileStorageService;

    public FileModel storeFile(MultipartFile file) {
        return fileStorageService.storeFile(file);
    }

    public byte[] getFile(String downloadId) throws IOException {
        FileModel resource = fileStorageService.loadFileAsResource(downloadId);
        return resource.getData();
    }

    public FileModel getFileModel(String filename) {
        return fileStorageService.loadFileAsResource(filename);
    }
//
//    public ResponseEntity<byte[]> getFile(String downloadId) throws IOException {
//        FileModel resource = fileStorageService.loadFileAsResource(downloadId);
//        String contentType = "application/octet-stream";
//
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFileName() + "\"")
//                .body(resource.getData());
//    }
}
