package pl.pranagal.bartosz.lcmsapp.file;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_FILE_GET;
import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_IMAGE_GET;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class FileLinkParser {
    public static String getLinkFromDownloadId(String downloadId){
        return ServletUriComponentsBuilder.fromCurrentContextPath().toUriString() + ROUTE_IMAGE_GET +"?filename=" + downloadId;
    }

    public static String getFileLinkFromDownloadId(String downloadId){
        return ServletUriComponentsBuilder.fromCurrentContextPath().toUriString() + ROUTE_FILE_GET +"?filename=" + downloadId;
    }

}
