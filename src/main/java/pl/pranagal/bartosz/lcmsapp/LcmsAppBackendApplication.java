package pl.pranagal.bartosz.lcmsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import pl.pranagal.bartosz.lcmsapp.configuration.FileStorageConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableConfigurationProperties({FileStorageConfiguration.class})
@SpringBootApplication
@EnableSwagger2
public class LcmsAppBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(LcmsAppBackendApplication.class, args);
    }

}
