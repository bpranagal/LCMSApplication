package pl.pranagal.bartosz.lcmsapp.role.model;

import lombok.*;
import pl.pranagal.bartosz.lcmsapp.user.model.Idenficiable;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "authorities")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class AuthorityEntity implements Idenficiable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(unique = true)
    private String name;

    @OneToMany(cascade = {CascadeType.DETACH, CascadeType.PERSIST}, mappedBy = "authority")
    private Set<UserEntity> users = new HashSet<>();

    public AuthorityEntity(String name) {
        this.name = name;
    }
}
