package pl.pranagal.bartosz.lcmsapp.role.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pranagal.bartosz.lcmsapp.role.model.AuthorityEntity;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<AuthorityEntity,Long> {
    Optional<AuthorityEntity> findByName(String name);
}
