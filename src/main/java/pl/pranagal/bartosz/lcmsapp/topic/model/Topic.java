package pl.pranagal.bartosz.lcmsapp.topic.model;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.topic.task.Task;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;


    private String title;

    private String topic;

    @Column(length = 2000)
    private String description;

    private Date date;

    private byte[] image;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Task task;

    @OneToMany(mappedBy = "topic", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private Set<FileModel> files = new HashSet<>();

    @Setter(AccessLevel.NONE)
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    public void setCourse(Course course){
        this.course = course;
        course.addTopic(this);
    }

    public void addFile(FileModel file){
        files.add(file);
        file.setTopic(this);
    }
}
