package pl.pranagal.bartosz.lcmsapp.topic.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TopicRequest {
    private String title;
    private String topic;
    private String description;
    private String courseId;
    private Date date;
}
