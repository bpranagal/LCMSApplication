package pl.pranagal.bartosz.lcmsapp.topic.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class Task implements Serializable {
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    private String title;
    private String description;
    private LocalDateTime dateTime;
}
