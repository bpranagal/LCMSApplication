package pl.pranagal.bartosz.lcmsapp.topic.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.file.FileResponse;
import pl.pranagal.bartosz.lcmsapp.topic.task.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TopicResponse {
    private Long id;
    private String title;
    private String topic;
    private String description;
    private Task task;
    private byte[] image;
    private List<FileResponse> files;
}
