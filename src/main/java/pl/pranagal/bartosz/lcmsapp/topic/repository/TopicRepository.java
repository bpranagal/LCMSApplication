package pl.pranagal.bartosz.lcmsapp.topic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pranagal.bartosz.lcmsapp.topic.model.Topic;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
    boolean existsByTitleAllIgnoreCase(String name);
}
