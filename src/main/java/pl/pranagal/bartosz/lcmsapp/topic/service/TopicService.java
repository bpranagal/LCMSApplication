package pl.pranagal.bartosz.lcmsapp.topic.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.pranagal.bartosz.lcmsapp.common.PermissionsService;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.course.service.CourseService;
import pl.pranagal.bartosz.lcmsapp.exception.topic.TopicAlreadyExistsException;
import pl.pranagal.bartosz.lcmsapp.exception.topic.TopicNotFound;
import pl.pranagal.bartosz.lcmsapp.file.FileMapper;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.file.FileResponse;
import pl.pranagal.bartosz.lcmsapp.file.FileStorageAdapter;
import pl.pranagal.bartosz.lcmsapp.topic.TopicMapper;
import pl.pranagal.bartosz.lcmsapp.topic.model.Topic;
import pl.pranagal.bartosz.lcmsapp.topic.repository.TopicRepository;
import pl.pranagal.bartosz.lcmsapp.topic.request.TopicRequest;
import pl.pranagal.bartosz.lcmsapp.topic.response.TopicResponse;
import pl.pranagal.bartosz.lcmsapp.topic.task.Task;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TopicService {
    private final TopicRepository topicRepository;
    private final FileStorageAdapter fileStorageAdapter;
    private final TopicMapper topicMapper;
    private final PermissionsService permissionsService;
    private final CourseService courseService;
    private final FileMapper fileMapper;

    public List<TopicResponse> getAll() {
        return topicRepository.findAll().stream()
                .map(topicMapper::entityToResponse)
                .collect(Collectors.toList());
    }

    public TopicResponse createTopic(TopicRequest topicRequest, Authentication authentication) {
        permissionsService.checkTopicToCourseAssign(authentication);

        Course course = courseService.findCourseById(topicRequest.getCourseId());
        Topic topic = topicMapper.requestToEntity(topicRequest);
        topic.setCourse(course);
        topicRepository.save(topic);

        return topicMapper.entityToResponse(topic);
    }

    public TopicResponse updateTopic(Long id, TopicRequest topicRequest){
        Topic existingTopic = topicRepository.findById(id).orElseThrow(() -> new TopicAlreadyExistsException(id.toString()));
        topicMapper.modifyEntityFromRequest(topicRequest,existingTopic);
        topicRepository.save(existingTopic);
        return topicMapper.entityToResponse(existingTopic);
    }

    public List<FileResponse> uploadFilesToTopic(MultipartFile[] multipartFiles, String id) {
        return uploadFiles(multipartFiles, findById(id));
    }

    private List<FileResponse> uploadFiles(MultipartFile[] multipartFiles, Topic topic) {
        List<FileModel> responses = Arrays.stream(multipartFiles)
                .map(multipartFile -> {
                    FileModel file = fileStorageAdapter.storeFile(multipartFile);
                    topic.addFile(file);
                    return file;
                })
                .collect(Collectors.toList());

        topicRepository.save(topic);

        return responses.stream().map(fileMapper::modelToResponse).collect(Collectors.toList());
    }

    public Topic findById(String id) {
        return topicRepository.findById(Long.parseLong(id))
                .orElseThrow(() -> new TopicNotFound(id + " id for topic was not found."));
    }

    public byte[] uploadImageToTopic(MultipartFile multipartFile, String id) throws IOException {
        Topic topic = findById(id);
        topic.setImage(multipartFile.getBytes());
        topicRepository.save(topic);

        return topic.getImage();
    }

    public void deleteTopic(String id) {
        Topic topic = findById(id);
        topicRepository.delete(topic);
    }

    public TopicResponse createTask(Task task, String id) {
        Topic topic = findById(id);
        topic.setTask(task);
        topicRepository.save(topic);

        return topicMapper.entityToResponse(topic);
    }

    public void deleteTask(String id) {
        Topic topic = findById(id);
        topic.setTask(null);
        topicRepository.save(topic);
    }
}
