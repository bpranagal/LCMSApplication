package pl.pranagal.bartosz.lcmsapp.topic;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import pl.pranagal.bartosz.lcmsapp.file.FileMapper;
import pl.pranagal.bartosz.lcmsapp.topic.model.Topic;
import pl.pranagal.bartosz.lcmsapp.topic.request.TopicRequest;
import pl.pranagal.bartosz.lcmsapp.topic.response.TopicResponse;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.request.UserEditRequest;

@Mapper(componentModel = "spring", uses = {FileMapper.class})
public interface TopicMapper {
    Topic requestToEntity(TopicRequest topicRequest);
    TopicResponse entityToResponse(Topic topic);
    public abstract void modifyEntityFromRequest(TopicRequest topicRequest, @MappingTarget Topic topic);
}
