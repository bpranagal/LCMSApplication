package pl.pranagal.bartosz.lcmsapp.exception.userexception;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.FieldNotCorrectException;

public class UsernameExistsException extends FieldNotCorrectException {
    public UsernameExistsException(String message) {
        super(message + ", given username already exists in our system.");
    }
}
