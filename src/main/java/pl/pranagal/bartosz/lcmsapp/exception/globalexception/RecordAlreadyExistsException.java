package pl.pranagal.bartosz.lcmsapp.exception.globalexception;

public class RecordAlreadyExistsException extends RuntimeException {
    public RecordAlreadyExistsException(String message) {
        super(message);
    }
}

