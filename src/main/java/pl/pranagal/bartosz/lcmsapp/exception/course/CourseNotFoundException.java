package pl.pranagal.bartosz.lcmsapp.exception.course;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.RecordNotFoundException;

public class CourseNotFoundException extends RecordNotFoundException {
    public CourseNotFoundException(String message) {
        super(message);
    }
}
