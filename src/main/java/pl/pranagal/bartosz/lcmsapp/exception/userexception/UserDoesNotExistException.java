package pl.pranagal.bartosz.lcmsapp.exception.userexception;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.RecordNotFoundException;

public class UserDoesNotExistException extends RecordNotFoundException {
    public UserDoesNotExistException(String message) {
        super("User with id: " + message + "doesn't exist.");
    }
}
