package pl.pranagal.bartosz.lcmsapp.exception.topic;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.RecordAlreadyExistsException;

public class TopicAlreadyExistsException extends RecordAlreadyExistsException {
    public TopicAlreadyExistsException(String message) {
        super(message);
    }
}
