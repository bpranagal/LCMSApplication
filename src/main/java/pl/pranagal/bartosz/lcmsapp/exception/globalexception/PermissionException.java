package pl.pranagal.bartosz.lcmsapp.exception.globalexception;

public class PermissionException extends RuntimeException{
    public PermissionException(String message) {
        super(message);
    }
}
