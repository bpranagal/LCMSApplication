package pl.pranagal.bartosz.lcmsapp.exception.course;

import java.nio.file.FileAlreadyExistsException;

public class CourseTitleExistsException extends FileAlreadyExistsException {
    public CourseTitleExistsException(String file) {
        super(file + " course with given title already exists.");
    }
}
