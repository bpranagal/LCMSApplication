package pl.pranagal.bartosz.lcmsapp.exception.topic;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.RecordNotFoundException;

public class TopicNotFound extends RecordNotFoundException {
    public TopicNotFound(String message) {
        super(message);
    }
}
