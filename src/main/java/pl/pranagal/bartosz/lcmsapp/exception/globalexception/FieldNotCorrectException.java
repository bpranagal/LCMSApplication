package pl.pranagal.bartosz.lcmsapp.exception.globalexception;

public class FieldNotCorrectException extends RuntimeException{
    public FieldNotCorrectException(String message){
        super(message);
    }
}
