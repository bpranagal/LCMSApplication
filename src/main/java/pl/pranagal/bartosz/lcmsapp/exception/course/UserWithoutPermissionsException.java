package pl.pranagal.bartosz.lcmsapp.exception.course;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.PermissionException;

public class UserWithoutPermissionsException extends PermissionException {
    public UserWithoutPermissionsException(String message) {
        super(message + " doesn't have permissions to perform this operation.");
    }
}
