package pl.pranagal.bartosz.lcmsapp.exception.globalexception;

public class RecordNotFoundException extends RuntimeException {
    public RecordNotFoundException(String message) {
        super(message);
    }
}
