package pl.pranagal.bartosz.lcmsapp.exception.userexception;

import pl.pranagal.bartosz.lcmsapp.exception.globalexception.FieldNotCorrectException;

public class EmailExistsException extends FieldNotCorrectException {
    public EmailExistsException(String message) {
        super(message + ", given mail already exists in our system.");
    }
}
