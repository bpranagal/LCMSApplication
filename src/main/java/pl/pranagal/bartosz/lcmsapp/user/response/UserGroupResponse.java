package pl.pranagal.bartosz.lcmsapp.user.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class UserGroupResponse {
    private Long groupId;
    private String groupName;
    private List<UserResponse> users;
}
