package pl.pranagal.bartosz.lcmsapp.user.model;

public enum UserRoleEnum {
    ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT
}
