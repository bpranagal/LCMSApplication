package pl.pranagal.bartosz.lcmsapp.user.response;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private String username;
    private String email;
    private String name;
    private String surname;
    private String role;
}
