package pl.pranagal.bartosz.lcmsapp.user.model;

public interface Idenficiable {
    Long getId();
}
