package pl.pranagal.bartosz.lcmsapp.user.request;

import lombok.*;
import pl.pranagal.bartosz.lcmsapp.user.model.UserRoleEnum;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateRequest extends UserDto {
    @NotBlank
    private String username;
    private String password;
    @NotBlank
    private String email;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
}
