package pl.pranagal.bartosz.lcmsapp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Override
    @Query("select u from UserEntity u left join fetch u.authority left join fetch u.group")
    List<UserEntity> findAll();

    Optional<UserEntity> findByUsername(String username);

    boolean existsByEmailAllIgnoreCase(String email);

    boolean existsByUsernameAllIgnoreCase(String username);

    @Override
    @Query("select u from UserEntity u left join fetch u.authority left join fetch u.group where u.id in :longs")
    List<UserEntity> findAllById(Iterable<Long> longs);
}
