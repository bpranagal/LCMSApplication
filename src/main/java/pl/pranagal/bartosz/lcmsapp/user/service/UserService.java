package pl.pranagal.bartosz.lcmsapp.user.service;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.pranagal.bartosz.lcmsapp.configuration.mapper.UserMapper;
import pl.pranagal.bartosz.lcmsapp.exception.userexception.EmailExistsException;
import pl.pranagal.bartosz.lcmsapp.exception.userexception.UserDoesNotExistException;
import pl.pranagal.bartosz.lcmsapp.exception.userexception.UsernameExistsException;
import pl.pranagal.bartosz.lcmsapp.group.model.Group;
import pl.pranagal.bartosz.lcmsapp.group.repository.GroupRepository;
import pl.pranagal.bartosz.lcmsapp.role.model.AuthorityEntity;
import pl.pranagal.bartosz.lcmsapp.role.repository.AuthorityRepository;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.model.UserRoleEnum;
import pl.pranagal.bartosz.lcmsapp.user.repository.UserRepository;
import pl.pranagal.bartosz.lcmsapp.user.request.UserCreateRequest;
import pl.pranagal.bartosz.lcmsapp.user.request.UserDto;
import pl.pranagal.bartosz.lcmsapp.user.request.UserEditRequest;
import pl.pranagal.bartosz.lcmsapp.user.request.UserRegisterRequest;
import pl.pranagal.bartosz.lcmsapp.user.response.UserGroupResponse;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final AuthorityRepository authorityRepository;
    private final GroupRepository groupRepository;

    @Transactional
    public UserResponse registerUser(UserRegisterRequest userRegisterRequest) {
        checkUserData(userRegisterRequest.getEmail(), userRegisterRequest.getUsername());
        checkAndSetUserRole(userRegisterRequest);
        UserEntity user = userMapper.createRegisterRequestToEntity(userRegisterRequest);
        user.setAuthority(getAuthorityByName(userRegisterRequest.getRole().toString()));
        groupRepository.getByNameIgnoreCase("Unasigned").ifPresent(group -> {
            user.setGroup(group);
            group.addUser(user);
        });
        userRepository.save(user);
        return userMapper.entityToResponse(user);
    }

    public UserResponse saveUser(UserCreateRequest userRequest) throws RuntimeException {
        checkUserData(userRequest.getEmail(), userRequest.getUsername());
        checkAndSetUserRole(userRequest);
        if (StringUtils.isBlank(userRequest.getPassword())) {
            userRequest.setPassword(generatePassword());
        }
        UserEntity user = userMapper.createRequestToEntity(userRequest);
        user.setAuthority(getAuthorityByName(userRequest.getRole().toString()));
        groupRepository.getByNameIgnoreCase("Unasigned").ifPresent(group -> {
            user.setGroup(group);
            group.addUser(user);
        });
        userRepository.save(user);
        return userMapper.entityToResponse(user);
    }

    private void checkAndSetUserRole(UserDto user) {
        if (Objects.isNull(user.getRole())) {
            user.setRole(UserRoleEnum.ROLE_STUDENT);
        }
    }

    private String generatePassword() {
        byte[] array = new byte[12];
        new Random().nextBytes(array);
        return new String(array, StandardCharsets.UTF_8);
    }

    private void checkUserData(String email, String username) {
        if (userRepository.existsByEmailAllIgnoreCase(email)) {
            throw new EmailExistsException(email);
        }
        if (userRepository.existsByUsernameAllIgnoreCase(username)) {
            throw new UsernameExistsException(username);
        }
    }

    public List<UserResponse> getUsers() {
        return userRepository.findAll().stream()
                .map(userMapper::entityToResponse)
                .collect(Collectors.toList());
    }

    public UserResponse updateUser(Long id, UserEditRequest userEditRequest) {
        UserEntity existingUser = userRepository.findById(id).orElseThrow(() -> new UserDoesNotExistException(id.toString()));
        if (!userEditRequest.getEmail().equalsIgnoreCase(existingUser.getEmail()) && userRepository.existsByEmailAllIgnoreCase(userEditRequest.getEmail())) {
            throw new EmailExistsException(userEditRequest.getEmail());
        }
        if (!userEditRequest.getUsername().equalsIgnoreCase(existingUser.getUsername()) && userRepository.existsByUsernameAllIgnoreCase(userEditRequest.getUsername())) {
            throw new UsernameExistsException(userEditRequest.getEmail());
        }
        userMapper.modifyEntityFromRequest(userEditRequest, existingUser);
        userRepository.save(existingUser);
        return userMapper.entityToResponse(existingUser);
    }

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = findUserByUsername(username);
        return new User(user.getUsername(), user.getPassword(), Collections.singleton(new SimpleGrantedAuthority(String.valueOf(user.getAuthority()))));
    }

    private AuthorityEntity getAuthorityByName(String name) {
        return authorityRepository.findByName(name)
                .orElseThrow(RuntimeException::new);
    }

    public void assignRole(Long userId, UserRoleEnum userRole) {
        UserEntity user = userRepository.findById(userId)
                .orElseThrow(() -> new UserDoesNotExistException(userId.toString()));

        user.setAuthority(getAuthorityByName(userRole.toString()));
        userRepository.save(user);
    }

    public UserEntity findUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    public List<UserEntity> findAllById(String[] userIds) {
        List<Long> ids = Stream.of(userIds).map(Long::parseLong).collect(Collectors.toList());
        return findAllById(ids);
    }

    public UserEntity findUserById(String userId) {
        return userRepository.findById(Long.parseLong(userId))
                .orElseThrow(() -> new UserDoesNotExistException(userId));
    }

    public List<UserGroupResponse> getGroupedUsers() {
        Map<Group, List<UserResponse>> usersMap = new TreeMap<>(Comparator.comparing(Group::getId));

        userRepository.findAll().forEach(userEntity -> {
            UserResponse response = userMapper.entityToResponse(userEntity);
            Group key = userEntity.getGroup();
            if (usersMap.containsKey(key)) {
                usersMap.get(key).add(response);
            } else {
                List<UserResponse> users = new ArrayList<>();
                users.add(response);
                usersMap.put(key, users);
            }
        });

        return usersMap.entrySet().stream().map(entry -> {
            UserGroupResponse response = new UserGroupResponse();
            response.setGroupId(entry.getKey().getId());
            response.setGroupName(entry.getKey().getName());
            response.setUsers(entry.getValue());
            return response;
        }).collect(Collectors.toList());
    }


    public List<UserEntity> findAllById(List<Long> userIds) {
        return userRepository.findAllById(userIds);
    }

    public void addImage(MultipartFile multipartFile, Authentication authentication) throws IOException {
        UserEntity user = findUserByUsername((String) authentication.getPrincipal());
        user.setImage(multipartFile.getBytes());
        userRepository.save(user);
    }
}
