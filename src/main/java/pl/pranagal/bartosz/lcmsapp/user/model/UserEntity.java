package pl.pranagal.bartosz.lcmsapp.user.model;

import lombok.*;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.group.model.Group;
import pl.pranagal.bartosz.lcmsapp.role.model.AuthorityEntity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "password", unique = true, nullable = false)
    private String password;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Setter(AccessLevel.NONE)
    @ManyToOne
    private Group group;

    @ManyToOne
    private AuthorityEntity authority;

    @OneToMany(mappedBy = "owner", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Course> ownedCourses = new HashSet<>();

    @ManyToMany(mappedBy = "participants", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Course> participatedCourses = new HashSet<>();

    @ManyToMany(mappedBy = "admins", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Course> adminCourses = new HashSet<>();

    private byte[] image;

    public void addOwnedCourse(Course course){
        ownedCourses.add(course);
        course.setOwner(this);
    }

    public void removeOwnedCourse(Course course){
        course.setOwner(null);
        ownedCourses.remove(course);
    }

    public void addParticipatedCourse(Course course){
        participatedCourses.add(course);
        course.addParticipant(this);
    }

    public void addAdminCourses(Course course){
        adminCourses.add(course);
        course.addAdmins(this);
    }

    public void removeAdminCourse(Course course) {
        adminCourses.remove(course);
        course.removeAdmin(this);
    }

    public void removeParticipatedCourse(Course course) {
        participatedCourses.remove(course);
        course.removeParticipant(this);
    }

    public void setGroup(Group group){
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity user = (UserEntity) o;

        if (!username.equals(user.username)) return false;
        return email.equals(user.email);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
