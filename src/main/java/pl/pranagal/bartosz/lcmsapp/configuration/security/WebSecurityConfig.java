package pl.pranagal.bartosz.lcmsapp.configuration.security;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.AuthorizationFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.*;


@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig{

    private final CustomAuthorizationFilter customAuthorizationFilter;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*", "**"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "*", "**"));
        configuration.setAllowedHeaders(Arrays.asList("Access-Control-Allow-Headers", "Access-Control-Allow-Origin",
                "Access-Control-Request-Method", "Access-Control-Request-Headers", "Origin",
                "Cache-Control", "Content-Type", "Authorization", "**", "*"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors();
        httpSecurity.csrf().disable();
        httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.authorizeRequests()
                .antMatchers(ROUTE_LOGIN).permitAll()
                .antMatchers(ROUTE_REGISTER).permitAll()
                .antMatchers(HttpMethod.GET, ROUTE_GROUP + "**").authenticated()
                .antMatchers(HttpMethod.POST, ROUTE_GROUP + "**").hasAnyAuthority(ROLE_ADMIN, ROLE_TEACHER)
                .antMatchers(HttpMethod.PUT, ROUTE_GROUP + "**").hasAnyAuthority(ROLE_ADMIN, ROLE_TEACHER)
                .antMatchers(HttpMethod.PUT, ROUTE_USER + ROUTE_ASSIGN_ROLE_ADMIN + "**").hasAnyAuthority(ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, ROUTE_COURSE + "/**").authenticated()
                .antMatchers(HttpMethod.GET, ROUTE_COURSE + "/**").authenticated()
                .antMatchers(HttpMethod.POST, ROUTE_COURSE + "/**").authenticated()
                .antMatchers(HttpMethod.GET, ROUTE_USER + "/**").authenticated()
                .antMatchers(HttpMethod.POST, ROUTE_USER + "/**").authenticated()
                .antMatchers(HttpMethod.PUT, ROUTE_USER + "/**").authenticated()
                .antMatchers(HttpMethod.DELETE, ROUTE_USER + "/**").authenticated()
                .anyRequest().permitAll();

        httpSecurity.addFilterBefore(customAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);

        return httpSecurity.build();
    }

    @Bean
    public PasswordEncoder getBcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}