package pl.pranagal.bartosz.lcmsapp.configuration;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.pranagal.bartosz.lcmsapp.group.model.Group;
import pl.pranagal.bartosz.lcmsapp.group.repository.GroupRepository;
import pl.pranagal.bartosz.lcmsapp.role.model.AuthorityEntity;
import pl.pranagal.bartosz.lcmsapp.role.repository.AuthorityRepository;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.model.UserRoleEnum;
import pl.pranagal.bartosz.lcmsapp.user.repository.UserRepository;
import pl.pranagal.bartosz.lcmsapp.user.request.UserRegisterRequest;
import pl.pranagal.bartosz.lcmsapp.user.service.UserService;

import java.util.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class DatabaseSeeder {

    private final AuthorityRepository authorityRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final GroupRepository groupRepository;

    //TODO dodaj grupe nauczyciele

    @EventListener(ApplicationReadyEvent.class)
    public void seed() {
        if (authorityRepository.findAll().isEmpty()) {
            List<AuthorityEntity> authorityList = new ArrayList<>();
            authorityList.add(new AuthorityEntity(UserRoleEnum.ROLE_STUDENT.toString()));
            authorityList.add(new AuthorityEntity(UserRoleEnum.ROLE_ADMIN.toString()));
            authorityList.add(new AuthorityEntity(UserRoleEnum.ROLE_TEACHER.toString()));
            authorityRepository.saveAll(authorityList);
        }
        if (userService.getUsers().isEmpty()) {

            if(!groupRepository.existsByNameAllIgnoreCase("Unasigned")){
                Group unasignedGroup = new Group();
                unasignedGroup.setName("Unasigned");
                unasignedGroup.setUsers(new HashSet<>());
                groupRepository.save(unasignedGroup);
            }

            Random rand = new Random();
            UserRegisterRequest admin = new UserRegisterRequest();
            admin.setRole(UserRoleEnum.ROLE_ADMIN);
            admin.setEmail("admin@gmail.com");
            admin.setName("admin");
            admin.setSurname("admin");
            admin.setUsername("admin");
            admin.setPassword("secret");
            userService.registerUser(admin);

            List<String> names = Arrays.asList("Stachu", "Monika", "Mirek", "Pawel", "Kacper", "Angelika", "Daniel", "Bartek", "Katarzyna", "Barbara", "Waldemar", "Milosz", "Staszek", "Gabriel", "Konrad", "Jakub", "Jaroslaw");
            List<String> surnames = Arrays.asList("Piszcz", "Pleszcz", "Deszcz", "Wieszcz", "Wiszcz", "Trzeszcz", "Kleszcz", "Niszcz", "Zgliszcz", "Pranagal", "Piorko", "Nowak", "Susek", "Pysusiewicz");
            for (int i = 0; i < 75; i++) {
                UserRegisterRequest userDto = new UserRegisterRequest();
                String name = names.get(rand.nextInt(names.size()));
                String surname = surnames.get(rand.nextInt(surnames.size()));
                String username = (name + surname).toLowerCase(Locale.ROOT) + rand.nextInt(100);
                String email = username + "@gmail.com";
                userDto.setName(name);
                userDto.setSurname(surname);
                userDto.setUsername(username);
                userDto.setEmail(email);
                userDto.setPassword("password");
                if (i % 2 == 0)
                    userDto.setRole(UserRoleEnum.ROLE_STUDENT);
                else
                    userDto.setRole(UserRoleEnum.ROLE_TEACHER);
                try {
                    userService.registerUser(userDto);
                } catch (Exception e) {
                    log.warn(e.getMessage());
                }
            }
        }
        if (!groupRepository.existsByNameAllIgnoreCase("Teachers")) {
            Group group = new Group();
            group.setName("Teachers");
            groupRepository.save(group);

            List<UserEntity> users = userRepository.findAll().stream()
                    .filter(user -> user.getAuthority().getName().equals(UserRoleEnum.ROLE_TEACHER.toString()))
                    .toList();

            users.forEach(group::addUser);

            userRepository.saveAll(users);
        }
    }

}
