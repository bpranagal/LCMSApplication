package pl.pranagal.bartosz.lcmsapp.configuration.mapper;

import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.pranagal.bartosz.lcmsapp.role.model.AuthorityEntity;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.request.UserCreateRequest;
import pl.pranagal.bartosz.lcmsapp.user.response.UserGroupResponse;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;
import pl.pranagal.bartosz.lcmsapp.user.request.UserEditRequest;
import pl.pranagal.bartosz.lcmsapp.user.request.UserRegisterRequest;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public abstract class UserMapper {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Mapping(source = "password", target = "password", qualifiedByName = "encodePassword")
    public abstract UserEntity createRequestToEntity(UserCreateRequest userRequest);

    @Mapping(source = "password", target = "password", qualifiedByName = "encodePassword")
    public abstract UserEntity createRegisterRequestToEntity(UserRegisterRequest userRequest);

    @Mapping(source = "authority", target = "role", qualifiedByName = "getRoleName")
    public abstract UserResponse entityToResponse(UserEntity userEntity);

    public abstract UserGroupResponse entityToUserGroupResponse(UserEntity userEntity);

    public abstract void modifyEntityFromRequest(UserEditRequest userEditRequest, @MappingTarget UserEntity user);

    @Named("encodePassword")
    String encodePassword(String password){
        return passwordEncoder.encode(password);
    }

    @Named("getRoleName")
    String getRoleName(AuthorityEntity role){
        return role.getName();
    }

}
