package pl.pranagal.bartosz.lcmsapp.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.pranagal.bartosz.lcmsapp.common.AppExceptionResponse;

import java.util.Arrays;
import java.util.Collections;

@Controller
@ControllerAdvice
@Slf4j
public class ExceptionConfig {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<AppExceptionResponse> handleException(RuntimeException e){
        log.error(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(AppExceptionResponse.builder()
                        .stacktrace(Collections.singletonList(Arrays.toString(e.getStackTrace())))
                        .error(e.getMessage())
                        .build());
    }


}
