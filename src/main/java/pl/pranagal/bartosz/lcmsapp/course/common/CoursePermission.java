package pl.pranagal.bartosz.lcmsapp.course.common;

public enum CoursePermission {
    OWNER, ADMIN, PARTICIPANT
}
