package pl.pranagal.bartosz.lcmsapp.course.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.expression.spel.ast.Assign;
import pl.pranagal.bartosz.lcmsapp.common.AssignTypeEnum;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CourseAssignRequest {
    private String courseId;
    private List<CourseUsersDetails> users;
    private AssignTypeEnum assignType;
}
