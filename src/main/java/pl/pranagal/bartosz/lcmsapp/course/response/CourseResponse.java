package pl.pranagal.bartosz.lcmsapp.course.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.topic.response.TopicResponse;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CourseResponse {
    private Long id;
    private String title;
    private String description;
    private UserResponse owner;
    private List<TopicResponse> topics;
    private byte[] image;
}
