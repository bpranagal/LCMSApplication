package pl.pranagal.bartosz.lcmsapp.course.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.topic.model.Topic;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", unique = true, nullable = false)
    private String title;

    @Column(name = "description", nullable = false, length = 2000)
    private String description;

    @Column(name = "image_path")
    private String imagePath;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private UserEntity owner;

    @JsonIgnore
    @ManyToMany
    private Set<UserEntity> participants = new HashSet<>();

    @JsonIgnore
    @ManyToMany
    private Set<UserEntity> admins = new HashSet<>();

    @OneToMany(mappedBy = "course", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private Set<Topic> topics = new HashSet<>();

    @Setter(AccessLevel.NONE)
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE})
    private FileModel file;

    public void setFile(FileModel fileModel){
        this.file = fileModel;
        fileModel.setCourse(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return title.equals(course.title);
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }

    public void addParticipant(UserEntity user) {
        participants.add(user);
    }

    public void addAdmins(UserEntity user) {
        admins.add(user);
    }

    public void removeAdmin(UserEntity user) {
        admins.remove(user);
    }

    public void removeParticipant(UserEntity user) {
        participants.remove(user);
    }

    public void addTopic(Topic topic) {
        topics.add(topic);
    }

}
