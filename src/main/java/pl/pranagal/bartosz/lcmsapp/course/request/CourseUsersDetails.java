package pl.pranagal.bartosz.lcmsapp.course.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pranagal.bartosz.lcmsapp.common.AssignTypeEnum;
import pl.pranagal.bartosz.lcmsapp.course.common.CoursePermission;

@Getter
@Setter
@NoArgsConstructor
public class CourseUsersDetails {
    private String id;
    private CoursePermission permission;
}
