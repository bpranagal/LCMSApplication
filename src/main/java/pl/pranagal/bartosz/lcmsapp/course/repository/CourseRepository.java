package pl.pranagal.bartosz.lcmsapp.course.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    boolean existsByTitleAllIgnoreCase(String title);

}
