package pl.pranagal.bartosz.lcmsapp.course;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.course.request.CourseRequest;
import pl.pranagal.bartosz.lcmsapp.course.response.CourseResponse;
import pl.pranagal.bartosz.lcmsapp.course.response.CourseWithoutTopicsResponse;
import pl.pranagal.bartosz.lcmsapp.file.FileMapper;
import pl.pranagal.bartosz.lcmsapp.file.FileRepository;
import pl.pranagal.bartosz.lcmsapp.topic.TopicMapper;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.request.UserEditRequest;

@Mapper(componentModel = "spring", uses = {TopicMapper.class, FileMapper.class})
public abstract class CourseMapper {
    public abstract Course requestToEntity(CourseRequest courseRequest);

    @Mapping(source = "course.file.downloadId", target = "image", qualifiedByName = "getImage")
    public abstract CourseResponse entityToResponse(Course course);

    @Mapping(source = "course.file.downloadId", target = "image", qualifiedByName = "getImage")
    public abstract CourseWithoutTopicsResponse entityToResponseNoTopic(Course course);

    public abstract void modifyEntityFromRequest(CourseRequest courseRequest, @MappingTarget Course course);
}
