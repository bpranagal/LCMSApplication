package pl.pranagal.bartosz.lcmsapp.course.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.pranagal.bartosz.lcmsapp.common.AssignTypeEnum;
import pl.pranagal.bartosz.lcmsapp.common.PermissionsService;
import pl.pranagal.bartosz.lcmsapp.configuration.mapper.UserMapper;
import pl.pranagal.bartosz.lcmsapp.course.CourseMapper;
import pl.pranagal.bartosz.lcmsapp.course.common.CoursePermission;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.course.repository.CourseRepository;
import pl.pranagal.bartosz.lcmsapp.course.request.CourseAssignRequest;
import pl.pranagal.bartosz.lcmsapp.course.request.CourseRequest;
import pl.pranagal.bartosz.lcmsapp.course.request.CourseUsersDetails;
import pl.pranagal.bartosz.lcmsapp.course.response.CourseResponse;
import pl.pranagal.bartosz.lcmsapp.course.response.CourseWithoutTopicsResponse;
import pl.pranagal.bartosz.lcmsapp.exception.course.CourseNotFoundException;
import pl.pranagal.bartosz.lcmsapp.exception.course.CourseTitleExistsException;
import pl.pranagal.bartosz.lcmsapp.exception.course.UserWithoutPermissionsException;
import pl.pranagal.bartosz.lcmsapp.file.FileMapper;
import pl.pranagal.bartosz.lcmsapp.file.FileModel;
import pl.pranagal.bartosz.lcmsapp.file.FileStorageAdapter;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.repository.UserRepository;
import pl.pranagal.bartosz.lcmsapp.user.response.UserResponse;
import pl.pranagal.bartosz.lcmsapp.user.service.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROLE_ADMIN;
import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROLE_STUDENT;

@Service
@RequiredArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;
    private final CourseMapper courseMapper;
    private final UserRepository userRepository;
    private final UserService userService;
    private final UserMapper userMapper;
    private final PermissionsService permissionsService;
    private final FileStorageAdapter fileStorageAdapter;
    private final FileMapper fileMapper;

    public List<CourseWithoutTopicsResponse> getAll() {
        return courseRepository.findAll().stream()
                .map(courseMapper::entityToResponseNoTopic)
                .collect(Collectors.toList());
    }

    public Course createCourse(CourseRequest courseRequest, MultipartFile multipartFile, String username) throws CourseTitleExistsException {
        UserEntity owner = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        checkCourseTitleExistence(courseRequest.getTitle());

        Course course = courseMapper.requestToEntity(courseRequest);
        course.setOwner(owner);
        courseRepository.save(course);

        manageUsers(new CourseAssignRequest(course.getId().toString(), courseRequest.getUsers(), AssignTypeEnum.ASSIGN), username);

        if (multipartFile != null) {
            FileModel fileModel = fileStorageAdapter.storeFile(multipartFile);
            course.setFile(fileModel);
            courseRepository.save(course);
        }

        return course;
    }

    @Transactional
    public void manageUsers(CourseAssignRequest request, String username) {
        Course course = findCourseById(request.getCourseId());
        checkAssignerPermissions(username, course);
        List<UserEntity> usersToAssign = new ArrayList<>();

        request.getUsers().forEach(user -> {
            UserEntity userEntity = userRepository.findById(Long.parseLong(user.getId()))
                    .orElse(null);
            manageUserAndAddToList(request, course, user, userEntity, usersToAssign);
        });

        if (usersToAssign.isEmpty())
            return;

        userRepository.saveAll(usersToAssign);
    }

    private void manageUserAndAddToList(CourseAssignRequest request, Course course, CourseUsersDetails user, UserEntity userEntity, List<UserEntity> users) {
        switch (request.getAssignType()) {
            case ASSIGN -> {
                if (user.getPermission().equals(CoursePermission.ADMIN)) {
                    if (!userEntity.getAuthority().getName().equals(ROLE_STUDENT))
                        userEntity.addAdminCourses(course);
                } else {
                    userEntity.addParticipatedCourse(course);
                }
            }
            case REMOVE -> {
                if (user.getPermission().equals(CoursePermission.ADMIN))
                    userEntity.removeAdminCourse(course);
                else
                    userEntity.removeParticipatedCourse(course);
            }
        }
        users.add(userEntity);
    }

    public void checkAssignerPermissions(String username, Course course) {
        UserEntity assigner = userService.findUserByUsername(username);
        if (!assigner.equals(course.getOwner()) && !course.getAdmins().contains(assigner) && !assigner.getAuthority().getName().equals(ROLE_ADMIN)) {
            throw new UserWithoutPermissionsException(username);
        }
    }

    public void checkCourseTitleExistence(String title) throws CourseTitleExistsException {
        if (courseRepository.existsByTitleAllIgnoreCase(title)) {
            throw new CourseTitleExistsException(title);
        }
    }

    public Course findCourseById(String courseId) {
        return courseRepository.findById(Long.parseLong(courseId))
                .orElseThrow(() -> new CourseNotFoundException("This course doesn't exists."));
    }

    public Map<CoursePermission, List<UserResponse>> getAllCourseUsers(String courseId) {
        Course course = findCourseById(courseId);

        List<UserResponse> admins = course.getAdmins().stream().map(userMapper::entityToResponse).collect(Collectors.toList());
        List<UserResponse> participants = course.getParticipants().stream().map(userMapper::entityToResponse).collect(Collectors.toList());
        UserResponse owner = userMapper.entityToResponse(course.getOwner());

        Map<CoursePermission, List<UserResponse>> users = new HashMap<>();
        users.put(CoursePermission.PARTICIPANT, participants);
        users.put(CoursePermission.ADMIN, admins);
        users.put(CoursePermission.OWNER, List.of(owner));

        return users;
    }

    public CourseResponse getCourseWithTopics(String id, Authentication authentication) {
        Course course = findCourseById(id);
        permissionsService.checkCoursePermission(course, authentication);
        return courseMapper.entityToResponse(course);
    }

    public byte[] uploadImage(String id, MultipartFile multipartFile, String principal) {
        Course course = findCourseById(id);

        FileModel fileModel = fileStorageAdapter.storeFile(multipartFile);
        course.setFile(fileModel);
        courseRepository.save(course);

        return course.getFile().getData();
    }

    public void deleteCourse(String id) {
        Course course = findCourseById(id);
        courseRepository.delete(course);
    }

    public CourseResponse updateCourse(Long id, CourseRequest courseRequest) throws CourseTitleExistsException {
        Course existingCourse = findCourseById(id.toString());
        if(!courseRequest.getTitle().equalsIgnoreCase(existingCourse.getTitle()) && courseRepository.existsByTitleAllIgnoreCase(courseRequest.getTitle())){
            throw new CourseTitleExistsException(courseRequest.getTitle());
        }
        courseMapper.modifyEntityFromRequest(courseRequest,existingCourse);
        courseRepository.save(existingCourse);
        return courseMapper.entityToResponse(existingCourse);
    }

}
