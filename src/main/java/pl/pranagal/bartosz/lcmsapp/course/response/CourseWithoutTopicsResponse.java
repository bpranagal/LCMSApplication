package pl.pranagal.bartosz.lcmsapp.course.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CourseWithoutTopicsResponse {
    private Long id;
    private String title;
    private String description;
    private byte[] image;
}
