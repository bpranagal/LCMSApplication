package pl.pranagal.bartosz.lcmsapp.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class ApplicationConstants {
    public static final String ROUTE_USER = "/user";
    public static final String ROUTE_LOGIN = "auth/login";
    public static final String ROUTE_REGISTER = "/register";
    public static final String ROUTE_GROUP = "/group";
    public static final String ROUTE_TOPIC = "/topic";
    public static final String ROUTE_ASSIGN_ROLE_ADMIN = "/assign/";
    public static final String ROUTE_COURSE = "/course";
    public static final String ROUTE_IMAGE_GET = "/image/get";
    public static final String ROUTE_FILE_GET = "/file/get";
    public static final String ROUTE_DELETE_FILE = "file/remove";
    public static final String ROUTE_COURSE_ASSIGN = "/assign/";
    public static final String ROUTE_COURSE_REMOVE = "/remove/";

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_TEACHER = "ROLE_TEACHER";
    public static final String ROLE_STUDENT = "ROLE_STUDENT";
}
