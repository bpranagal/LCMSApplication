package pl.pranagal.bartosz.lcmsapp.common;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.pranagal.bartosz.lcmsapp.course.model.Course;
import pl.pranagal.bartosz.lcmsapp.exception.course.UserWithoutPermissionsException;
import pl.pranagal.bartosz.lcmsapp.user.model.UserEntity;
import pl.pranagal.bartosz.lcmsapp.user.service.UserService;

import java.util.Optional;

import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.*;

@Service
@RequiredArgsConstructor
public class PermissionsService {
    private final UserService userService;

    public void checkTopicToCourseAssign(Authentication authentication){

    }

    public void checkGroupAssignerPermissions(Authentication authentication) {
        Optional<?> role = authentication.getAuthorities().stream().findFirst();
        if (role.isEmpty() || !role.get().toString().equals(ROLE_ADMIN)) {
            throw new UserWithoutPermissionsException(authentication.getName());
        }
    }

    public void checkCoursePermission(Course course, Authentication authentication) {
        UserEntity user = userService.findUserByUsername((String) authentication.getPrincipal());
        if(!course.getAdmins().contains(course) && !course.getOwner().equals(user) && !course.getParticipants().contains(user) && !user.getAuthority().getName().equals(ROLE_ADMIN)){
            throw new UserWithoutPermissionsException(authentication.getName());
        }
    }
}
