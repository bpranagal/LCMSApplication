package pl.pranagal.bartosz.lcmsapp.common;

import lombok.Data;

@Data
public class LoginRequest {

    private String username;
    private String password;
}
