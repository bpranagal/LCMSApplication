package pl.pranagal.bartosz.lcmsapp.common;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AppExceptionResponse {
    private String error;
    private List<String> stacktrace;
}
