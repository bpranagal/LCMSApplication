package pl.pranagal.bartosz.lcmsapp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import pl.pranagal.bartosz.lcmsapp.configuration.FileStorageConfiguration;

@SpringBootTest
class LcmsAppBackendApplicationTests {

    @Test
    void contextLoads() {
    }

}
