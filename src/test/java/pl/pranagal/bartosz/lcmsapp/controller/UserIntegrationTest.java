//package pl.pranagal.bartosz.lcmsapp.controller;
//
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.Test;
//
//import org.testcontainers.containers.PostgreSQLContainer;
//
//
//import org.testcontainers.junit.jupiter.Container;
//import org.testcontainers.junit.jupiter.Testcontainers;
//import org.junit.ClassRule;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.DynamicPropertyRegistry;
//import org.springframework.test.context.DynamicPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.transaction.annotation.Transactional;
//import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
//import pl.pranagal.bartosz.lcmsapp.user.model.UserRoleEnum;
//import pl.pranagal.bartosz.lcmsapp.user.request.UserCreateRequest;
//import pl.pranagal.bartosz.lcmsapp.user.service.UserService;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_ASSIGN_ROLE_ADMIN;
//import static pl.pranagal.bartosz.lcmsapp.common.ApplicationConstants.ROUTE_USER;
//
//@AutoConfigureMockMvc
//@Transactional
//@Testcontainers
//@RunWith(SpringRunner.class)
//@SpringBootTest
//class UserIntegrationTest {
//
//    @Container
//    private static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:latest");
//
//    @Autowired
//    MockMvc mvc;
//
//    @Autowired
//    private UserService userService;
//
//    @DynamicPropertySource
//    static void registerMySQLProperties(DynamicPropertyRegistry registry) {
//        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
//        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
//        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
//    }
//
//    @Test
//    @WithMockUser(value = "student", roles = "USER")
//    void checkRoleAssignFailed() throws Exception {
//        mvc.perform(MockMvcRequestBuilders.put(ROUTE_USER + ROUTE_ASSIGN_ROLE_ADMIN + "/1")
//                        .param("userRole", UserRoleEnum.ROLE_STUDENT.toString()))
//                .andDo(print())
//                .andExpect(status().isForbidden());
//    }
//
//    @Test
//    @WithMockUser(value = "student", roles = "ADMIN")
//    void checkRoleAssignPassed() throws Exception {
//        UserCreateRequest userCreateRequest = new UserCreateRequest();
//        userCreateRequest.setRole(UserRoleEnum.ROLE_STUDENT);
//        userCreateRequest.setUsername("stachu");
//        userCreateRequest.setEmail("stachu");
//        userCreateRequest.setName("stachu");
//        userCreateRequest.setSurname("stachu");
//        userCreateRequest.setPassword("stachu");
//        userService.saveUser(userCreateRequest);
//        mvc.perform(MockMvcRequestBuilders.put(ROUTE_USER + ROUTE_ASSIGN_ROLE_ADMIN + "/1")
//                        .param("userRole", UserRoleEnum.ROLE_STUDENT.toString()))
//                .andDo(print())
//                .andExpect(status().isNoContent());
//    }
//}
